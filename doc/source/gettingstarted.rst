Getting Started
===============

Prerequisites
-------------

The only dependency for thorlabs_mc2000b is the python serial library
(`pyserial <https://pypi.org/project/pyserial/>`_), which should be installed automatically if
using pip or similar.
If obtaining the code by other means, ensure it is installed and can be found in your python path.

Installing the Software
-----------------------

Download Using Pip
^^^^^^^^^^^^^^^^^^

The package installer for Python (pip) is the typical method for installation:

.. code-block:: sh

    pip install --user --upgrade thorlabs-mc2000b

The ``--user`` parameter installs using user-level permissions, thus does not require root or administrator privileges.
To install system wide, remove the ``--user`` parameter and prefix the command with ``sudo`` (Linux, MacOS), or run as administrator (Windows).

Clone From Git
^^^^^^^^^^^^^^

Alternatively, the latest version can be downloaded from the git repository:

.. code-block:: sh

    git clone https://gitlab.com/ptapping/thorlabs-mc2000b.git

and optionally installed to your system using ``pip``:

.. code-block:: sh

    pip install --user ./thorlabs-mc2000b


Usage
-----

.. code-block:: python

    from thorlabs_mc2000b import MC2000B, Blade
    # Initialise the first detected device
    chopper = MC2000B()
    # We'll assume the default MC1F10HP is installed
    print(chopper.get_blade_string())
    # Set up to use external reference source and the inner part of the blade
    chopper.set_inref_string("external-inner")
    # Apply a 1/2 divider to the input frequency
    chopper.nharmonic = 1
    chopper.dharmonic = 2
    # Start it up!
    chopper.enable = True

    # If a different chopper blade is installed, it can be configured by
    # using the Blade enum, for example for the MC1F60 model:
    chopper.blade = Blade.MC1F60
    # This may change the available input and/or output reference sources.
    # To check which are available for a blade model:
    print(Blade.MC1F60.inrefs)
    print(Blade.MC1F60.outrefs)





