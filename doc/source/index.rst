Welcome to thorlabs_mc2000b's documentation!
===============================================

This is an interface to the Thorlabs MC2000B optical chopper unit, communicating over the USB
serial port.

Information about the unit can be found on the `product webpage <https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=287>`_,
and details of the commands and how they apply to the different models of chopper blades can be
found in the `user manual <https://www.thorlabs.com/_sd.cfm?fileName=TTN102010-D02.pdf&partNumber=MC2000B-EC>`_.

User Guide
----------

.. toctree::
   :maxdepth: 2

   gettingstarted
   development

API Documentation
-----------------
.. toctree::
   :maxdepth: 5

   api/thorlabs_mc2000b


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
